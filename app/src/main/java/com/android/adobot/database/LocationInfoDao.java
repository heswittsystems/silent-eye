package com.android.adobot.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


@Dao
public interface LocationInfoDao {

        @Insert
        void insert(LocationInfo locationInfo);

        @Query("SELECT * FROM locationinfo ")
        LocationInfo[] all();

        @Query("DELETE FROM locationinfo")
        void delete();

        @Query("DELETE FROM locationinfo WHERE id = :id")
        void deleteOne(int id);

}

