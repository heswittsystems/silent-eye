package com.android.adobot.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface UninstallDao {

        @Insert
        void insert(Uninstall uninstall);

        @Query("SELECT * FROM uninstall ")
        List<Uninstall> all();

        @Query("DELETE FROM uninstall")
        void delete();

        @Query("DELETE FROM uninstall WHERE id = :id")
        void deleteOne(int id);

}

